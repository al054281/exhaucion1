package exhauscion;

public class Exhaucion {
//PAUL MAS QUI ISC "IA"
    public static double aux;

    public static void Operaciones(int lados, double inicio) {
        int N = lados;
        double LadoInicial = inicio, X, D, E, F, G, H, a, b, c, d, e;
        X = LadoInicial / 2;
        a = Math.pow(X, 2);
        D = Math.sqrt(1 - a);
        E = 1 - D;
        b = Math.pow(E, 2);
        F = Math.sqrt(a + b);
        G = N * LadoInicial;
        H = G / 2;

        aux = F;
        System.out.println("Numero de lados: " + N + "\n" + "Lado Inicial: " + LadoInicial + "\n" + "X: " + X + "\n" + "a : " + D + "\n" + "b : " + E + "\n" + "Nuevo Lado: " + F + "\n" + "P: " + G + "\n" + "P/D: " + H);
        System.out.println("El numero PI es: " + H);
    }

    public static void main(String[] args) {
        int L = 6;
        double I = 1;

        for (int i = 0; i < 20; i++) {
            System.out.println("paso # " + (1 + i));
            Operaciones(L, I);
            L = L * 2;
            I = aux;
            System.out.println("----------------------");
        }
    }
}
